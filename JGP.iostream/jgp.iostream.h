#include "FileBuffer.h"
#include <vector>

#ifdef JGPIOSTREAM_EXPORTS
#    define JGPIOSTREAM_DLLCALL __declspec(dllexport)
#else
#    define JGPIOSTREAM_DLLCALL __declspec(dllimport)
#endif

JGPIOSTREAM_DLLCALL std::vector<const char*>* jgp_iostr_fileListing(const char *path, bool includePath);

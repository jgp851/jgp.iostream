#include "jgp.iostream.h"
#include <dirent.h>
#include <string>

std::vector<const char*>* jgp_iostr_fileListing(const char *path, bool includePath)
{
	std::vector<const char*> *files = new std::vector<const char*>();
	struct dirent *file;
	DIR *dirPath = opendir(path);
	
	while((file = readdir(dirPath)))
	{
		// uzyskanie odpowiedniej �cie�ki
		std::string filePath = "";
		if(includePath)
			filePath = path;
		filePath += file->d_name;
		// skopiowanie stringa do char* i dodanie do files
		char* str = new char[filePath.size()];
		strcpy(str, filePath.c_str());
		files->push_back(str);
	}
	
	closedir(dirPath);
	
	files->erase(files->begin(), files->begin() + 2);

	delete file;
	return files;
}
#pragma once
#include <string>
#include <fstream>

#ifdef JGPIOSTREAM_EXPORTS
#    define JGPIOSTREAM_DLLCALL __declspec(dllexport)
#else
#    define JGPIOSTREAM_DLLCALL __declspec(dllimport)
#endif

class JGP_IOSTREAM_FileBuffer
{
private:
	char *fileBuffer;
	unsigned int fileLength;
	std::string fileType;
public:
	JGPIOSTREAM_DLLCALL JGP_IOSTREAM_FileBuffer(std::string path);
	JGPIOSTREAM_DLLCALL JGP_IOSTREAM_FileBuffer(std::ifstream &file);
	JGPIOSTREAM_DLLCALL ~JGP_IOSTREAM_FileBuffer();

	JGPIOSTREAM_DLLCALL char* getFileBuffer();
	JGPIOSTREAM_DLLCALL const char* getFileType();
	JGPIOSTREAM_DLLCALL unsigned int getFileLength();
	JGPIOSTREAM_DLLCALL void readFile(std::ifstream &file);
	JGPIOSTREAM_DLLCALL void writeFile(std::ofstream &file);
};
#include "FileBuffer.h"

using namespace std;

JGP_IOSTREAM_FileBuffer::JGP_IOSTREAM_FileBuffer(std::string path)
{
	fileType.substr(fileType.rfind("."));
	ifstream file(path.c_str(), ios::binary);
	file.seekg(ios::end);
	fileLength = (unsigned int)file.tellg();
	file.seekg(ios::beg);
	fileBuffer = new char[fileLength];
	file.read(fileBuffer, fileLength);
	file.close();
}
JGP_IOSTREAM_FileBuffer::JGP_IOSTREAM_FileBuffer(ifstream &file)
{
	fileBuffer = NULL;
	readFile(file);
}
JGP_IOSTREAM_FileBuffer::~JGP_IOSTREAM_FileBuffer()
{
	if(fileBuffer)
		delete[] fileBuffer;
}

char* JGP_IOSTREAM_FileBuffer::getFileBuffer()
{
	return fileBuffer;
}

const char* JGP_IOSTREAM_FileBuffer::getFileType()
{
	return fileType.c_str();
}

unsigned int JGP_IOSTREAM_FileBuffer::getFileLength()
{
	return fileLength;
}

void JGP_IOSTREAM_FileBuffer::readFile(ifstream &file)
{
	if(fileBuffer)
		delete[] fileBuffer;
	// typ pliku
	char tmpType[6];
	unsigned char str_len = fileType.size();
	file.read((char*)&str_len, 1);
	file.read(tmpType, str_len);
	tmpType[str_len] = '\0';
	fileType = tmpType;
	// plik
	file.read((char*)&fileLength, sizeof(unsigned int));
	fileBuffer = new char[fileLength];
	file.read(fileBuffer, fileLength);
}

void JGP_IOSTREAM_FileBuffer::writeFile(ofstream &file)
{
	// typ pliku
	unsigned char value_len;
	value_len = fileType.size();
	file.write((char*)&value_len, 1);
	file.write(fileType.c_str(), value_len);
	// plik
	file.write((char*)&fileLength, sizeof(unsigned int));
	file.write(fileBuffer, fileLength);
}